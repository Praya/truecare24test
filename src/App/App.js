import React, { Component } from 'react';
import './App.css';
import Sidebar from './Sidebar/Sidebar'
import Content from './common/Content/Content';
import Topbar from './Topbar/Topbar';
import Providers from './pages/Providers/Providers';

class App extends Component {
  render() {
    return (
      <div className="App flex-row">
      
        <Sidebar className="flex-row" />

        <div className="main-content flex-column">
          <Topbar/>
          <Content title="Matched providers" page={Providers} />
        </div>

      </div>
    );
  }
}

export default App;
