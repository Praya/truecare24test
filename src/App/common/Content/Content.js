import React, { Component } from 'react';
import './Content.css';

class Content extends Component {
  render() {

    const Component = this.props.page;

    return (
      <div className="Content flex-column">
        <div className="Content-page-title">{this.props.title}</div>
        <Component/>
      </div>
    );
  }
}

export default Content;
