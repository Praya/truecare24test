import React from 'react';
import ReactDOM from 'react-dom';
import ProviderTable from './ProviderTable';
import { generateProviders } from '../../providers';

it('renders without crashing', () => {
  const div = document.createElement('div');
  const providers = generateProviders();

  ReactDOM.render(<ProviderTable providers={providers} />, div);
  ReactDOM.unmountComponentAtNode(div);
});
