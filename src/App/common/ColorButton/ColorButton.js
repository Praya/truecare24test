import React, { Component } from 'react';
import './ColorButton.css';

class ColorButton extends Component {

  onClick(event) {
    if (this.props.onClick) {
      this.props.onClick(event);
    }
  }

  render() {
    const color = this.props.color;
    return (
      <button
        className={`ColorButton ColorButton_${color}`}
        onClick={event => this.onClick(event)}>{this.props.text}</button>
    );
  }

}

export default ColorButton;
