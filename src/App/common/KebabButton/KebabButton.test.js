import React from 'react';
import ReactDOM from 'react-dom';
import KebabButton from './KebabButton';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<KebabButton status="status" />, div);
  ReactDOM.unmountComponentAtNode(div);
});
