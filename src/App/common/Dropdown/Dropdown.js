import React, { Component } from 'react';
import './Dropdown.css';

class Dropdown extends Component {

  constructor() {
    super();
    this.state = {isActive: false}

    this.globalClickHandler = event => {
      if (this.state.isActive && !event.target.matches(`#${this.id} *`)) {
        this.setState({isActive: false});
      }
    }

    this.triggerHandler = event => {
      this.setState({isActive: true});
    }

    Dropdown.counter  = Dropdown.counter ? Dropdown.counter + 1 : 1;

    this.id = `Dropdown-${Dropdown.counter}`;

  }

  componentWillMount() {
    window.addEventListener('click', this.globalClickHandler);
  }

  componentWillUnmount() {
    window.removeEventListener('click', this.globalClickHandler);
  }

  onOptionSelected(item) {
    this.setState({isActive: false});
    this.props.onSelect(item);
  }

  Options() {

    const Component = this.props.component;

    return this.props.items.map(item => (
      <div
        className="Dropdown-opiton"
        key={item[this.props.keyName]}
        onClick={event => this.onOptionSelected(item)}
        >
          <Component item={item} />
      </div>
    ));
  }

  Menu() {
    return <div className="Dropdown-menu">{this.Options()}</div>;
  }

  render() {
    return (
      <div id={this.id} className={'Dropdown ' + this.props.className}>
        <div className="Dropdown-label" onClick={this.triggerHandler}>
          {this.props.label}
        </div>
        {this.state.isActive && this.Menu()}
      </div>
    );
  }
}

export default Dropdown;
