import React from 'react';
import ReactDOM from 'react-dom';
import DropdownSelector from './DropdownSelector';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<DropdownSelector status="status" />, div);
  ReactDOM.unmountComponentAtNode(div);
});
