import React, { Component } from 'react';
import Dropdown from '../Dropdown/Dropdown';

class DropdownSelector extends Component {

  render() {
    const Component = this.props.component;
    const label = <Component item={this.props.current} />;

    return (
      <Dropdown
        className={this.props.className}
        label={label}
        keyName={this.props.keyName}
        items={this.props.items}
        component={Component}
        onSelect={this.props.onSelect}
        />
    );
  }

}

export default DropdownSelector;
