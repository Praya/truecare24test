import React, { Component } from 'react';
import './Rating.css';
import RatingStar from "./RatingStar/RatingStar";

class Rating extends Component {
  render() {
    return (
      <div className="Rating">
        <RatingStar active={this.props.rating >= 1}/>
        <RatingStar active={this.props.rating >= 2}/>
        <RatingStar active={this.props.rating >= 3}/>
        <RatingStar active={this.props.rating >= 4}/>
        <RatingStar active={this.props.rating >= 5}/>
      </div>
    );
  }
}

export default Rating;
