import React, { Component } from 'react';
import './RatingStar.css';

class RatingStar extends Component {
  render() {
    const color = this.props.active ? 'yellow' : 'grey';
    return <img className="RatingStar" src={`icons/star-${color}.svg`} />;
  }
}

export default RatingStar;
