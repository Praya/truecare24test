import React, { Component } from 'react';
import './ModalWindow.css';

class ModalWindow extends Component {
  render() {
    return (
      <div className={'ModalWindow ' + this.props.className}>
        <div className="ModalWindow-shadow" onClick={event => this.props.onClose(event)}/>
        <div className="ModalWindow-window">
          <div className="ModalWindow-title">{this.props.title}</div>
          <div className="ModalWindow-close" onClick={event => this.props.onClose(event)}>×</div>
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default ModalWindow;
