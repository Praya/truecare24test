import React, { Component } from 'react';
import './GoBack.css';

class GoBack extends Component {
  render() {
    return (
      <div className="GoBack flex-row">
        <img className="GoBack-image" src="icons/back.svg" alt="back" />
        <div>Back</div>
      </div>
    );
  }
}

export default GoBack;
