import React from 'react';
import ReactDOM from 'react-dom';
import Fulfillment from './Fulfillment';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Fulfillment/>, div);
  ReactDOM.unmountComponentAtNode(div);
});
