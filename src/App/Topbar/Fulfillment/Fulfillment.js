import React, { Component } from 'react';
import './Fulfillment.css';

class Fulfillment extends Component {
  render() {
    return (
      <div className="Fulfillment flex-row">
        <img className="Fulfillment-image" src="icons/fulfillment-kpis.svg" alt="fulfillment" />
        <div className="Fulfillment-title">Fulfillment KPIs</div>
      </div>
    );
  }
}

export default Fulfillment;
