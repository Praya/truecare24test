import React, { Component } from 'react';
import './User.css';

class User extends Component {
  render() {
    return (
      <div className="User flex-row">
        <div className="User-email">leo@truecare24.com</div>
        <img className="User-dropdown-icon" src="icons/dropdown.svg" alt="dropdown" />
      </div>
    );
  }
}

export default User;
