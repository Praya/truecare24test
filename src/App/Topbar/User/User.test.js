import React from 'react';
import ReactDOM from 'react-dom';
import GoBack from './GoBack';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<GoBack/>, div);
  ReactDOM.unmountComponentAtNode(div);
});
