import React, { Component } from 'react';
import './Topbar.css';
import GoToCase from './GoToCase/GoToCase';
import GoBack from './GoBack/GoBack';
import Fulfillment from './Fulfillment/Fulfillment';
import User from './User/User';

class Topbar extends Component {
  render() {
    return (
      <div className="Topbar flex-row">
        <GoBack/>
        <GoToCase/>
        <Fulfillment/>
        <div className="flex-spring"></div>
        <User/>
      </div>
    );
  }
}

export default Topbar;
