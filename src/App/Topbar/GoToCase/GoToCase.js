import React, { Component } from 'react';
import './GoToCase.css';

class GoToCase extends Component {
  render() {
    return (
      <div className="GoToCase flex-row">
        <input className="GoToCase-input" placeholder="Go to case ID#" />
        <img className="GoToCase-go" src="icons/go.svg" alt="go" />
      </div>
    );
  }
}

export default GoToCase;
