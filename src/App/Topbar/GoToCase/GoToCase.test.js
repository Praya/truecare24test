import React from 'react';
import ReactDOM from 'react-dom';
import GoToCase from './GoToCase';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<GoToCase/>, div);
  ReactDOM.unmountComponentAtNode(div);
});
