import React, { Component } from 'react';
import './ProviderTable.css';
import ProviderRow from './ProviderRow/ProviderRow';

class ProviderTable extends Component {
  render() {

    const providers = this.props.providers;

    return (
      <table className="ProviderTable">
        <thead>
          <tr>
            <th colSpan="2">Type</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>ID</th>
            <th>Status</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {
            providers.map(provider => (
              <ProviderRow onFeedback={this.props.onFeedback} key={provider.id} provider={provider} />
            ))
          }
        </tbody>
      </table>
    );
  }

}

export default ProviderTable;
