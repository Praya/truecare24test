import React from 'react';
import ReactDOM from 'react-dom';
import ProviderRow from './ProviderRow';
import { generateProviders } from '../../providers';

it('renders without crashing', () => {
  const tbody = document.createElement('tbody');
  const [provider] = generateProviders(1);

  ReactDOM.render(<ProviderRow provider={provider} />, tbody);
  ReactDOM.unmountComponentAtNode(tbody);
});
