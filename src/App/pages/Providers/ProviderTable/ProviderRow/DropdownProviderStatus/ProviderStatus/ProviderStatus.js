import React, { Component } from 'react';
import './ProviderStatus.css';

class ProviderStatus extends Component {
  render() {
    return (
      <div className="ProviderStatus flex-row">
        <div className="ProviderStatus-icon">
          <img src={`icons/${this.props.item.key}.svg`} alt={this.props.item.text} />
        </div>
        <div className="ProviderStatus-text">{this.props.item.text}</div>
      </div>
    );
  }
}

export default ProviderStatus;
