import React from 'react';
import ReactDOM from 'react-dom';
import ProviderStatus from './ProviderStatus';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ProviderStatus status="status" />, div);
  ReactDOM.unmountComponentAtNode(div);
});
