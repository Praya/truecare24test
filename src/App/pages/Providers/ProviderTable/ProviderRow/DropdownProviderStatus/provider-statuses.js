export const providerStatuses = [
  {
    key: 'contacting',
    text: 'Contacting'
  },
  {
    key: 'talked-to-client',
    text: 'Talked to the client'
  },
  {
    key: 'assessment-scheduled',
    text: 'Assessment scheduled'
  },
  {
    key: 'contract-signed',
    text: 'Contract signed'
  },
  {
    key: 'cancel-client',
    text: 'Cancel the client'
  },
];