import React, { Component } from 'react';
import './DropdownProviderStatus.css';
import DropdownSelector from '../../../../../common/DropdownSelector/DropdownSelector';
import ProviderStatus from './ProviderStatus/ProviderStatus';
import { providerStatuses } from './provider-statuses';


class DropdownProviderStatus extends Component {

  constructor(props) {
    super(props);
    this.state = {status: props.status};
  }
  
  render() {
    const current = providerStatuses.find(status => status.key === this.state.status);
    return <DropdownSelector
      className="DropdownProviderStatus"
      keyName="key"
      current={current}
      items={providerStatuses}
      component={ProviderStatus}
      onSelect={status => this.setState({status: status.key})}
      />
  }
}

export default DropdownProviderStatus;
