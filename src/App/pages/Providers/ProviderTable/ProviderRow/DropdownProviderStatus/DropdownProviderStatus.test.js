import React from 'react';
import ReactDOM from 'react-dom';
import DropdownProviderStatus from './DropdownProviderStatus';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<DropdownProviderStatus/>, div);
  ReactDOM.unmountComponentAtNode(div);
});
