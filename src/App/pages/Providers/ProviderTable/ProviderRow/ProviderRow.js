import React, { Component } from 'react';
import './ProviderRow.css';
import DropdownProviderStatus from './DropdownProviderStatus/DropdownProviderStatus';
import ProviderControls from './ProviderControls/ProviderControls';
import {makeCall} from "../../../../../makeCall";


class ProviderRow extends Component {

  handleAction(action) {
    switch (action) {
      case 'call':
        makeCall(this.props.provider);
        break;
      case 'provide-feedback':
        this.props.onFeedback(this.props.provider);
        break;
      default:
        console.warn(`Unknown action "${action}"`);
        break;
    }
  }

  className() {
    const classNames = ['ProviderRow'];

    if (this.props.provider.status === 'cancel-client') {
      classNames.push('ProviderRow-cancel');
    }

    return classNames.join(' ');
  }

  render() {
    return (
      <tr className={this.className()}>
        <td className="ProviderRow-type-1">
          <img src={`icons/${this.props.provider.contract}.svg`} alt={this.props.provider.contract} />
        </td>
        <td className="ProviderRow-type-2">
          <img src={`icons/${this.props.provider.type}.svg`} alt={this.props.provider.type} />
        </td>
        <td className="ProviderRow-name">
          {this.props.provider.firstName} {this.props.provider.lastName}
        </td>
        <td className="ProviderRow-email">{this.props.provider.email}</td>
        <td className="ProviderRow-phone">{this.props.provider.phone}</td>
        <td className="ProviderRow-id">{this.props.provider.id}</td>
        <td className="ProviderRow-status">
          <DropdownProviderStatus status={this.props.provider.status} />
        </td>
        <td className="ProviderRow-controls">
          <ProviderControls onAction={action => this.handleAction(action)}/>
        </td>
      </tr>
    );
  }

}

export default ProviderRow;
