import React, { Component } from 'react';
import './ProviderControls.css';
import ColorButton from '../../../../../common/ColorButton/ColorButton';
import DropdownProviderActions from '../DropdownProviderActions/DropdownProviderActions';

class ProviderControls extends Component {
  render() {
    return (
      <div className="ProviderControls flex-row">
        <ColorButton text="Chat" onClick={() => this.props.onAction('chat')} color="blue" />
        <ColorButton text="Call" onClick={() => this.props.onAction('call')} color="green" />
        <DropdownProviderActions onAction={this.props.onAction}/>
      </div>
    );
  }
}

export default ProviderControls;
