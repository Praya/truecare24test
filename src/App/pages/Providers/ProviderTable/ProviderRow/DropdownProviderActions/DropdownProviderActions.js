import React, { Component } from 'react';
import './DropdownProviderActions.css';
import Dropdown from '../../../../../common/Dropdown/Dropdown';
import KebabButton from '../../../../../common/KebabButton/KebabButton';

class DropdownProviderActions extends Component {

  actions() {
    return [
      {
        key: 'send-intro',
        text: 'Send intro',
      },
      {
        key: 'provide-feedback',
        text: 'Provide feedback',
      },
      {
        key: 'send-reminder',
        text: 'Send reminder',
      },
      {
        key: 'background-check',
        text: 'Background check',
      },
    ];
  }

  render() {
    return (
      <div className="DropdownProviderActions">
        <Dropdown
          label={<KebabButton/>}
          keyName="key"
          items={this.actions()}
          component={props => <div className="DropdownProviderActions-action">{props.item.text}</div>}
          onSelect={action => this.props.onAction(action.key)}
          />
      </div>
    );
  }

}

export default DropdownProviderActions;
