import React from 'react';
import ReactDOM from 'react-dom';
import DropdownProviderActions from './DropdownProviderActions';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<DropdownProviderActions status="status" />, div);
  ReactDOM.unmountComponentAtNode(div);
});
