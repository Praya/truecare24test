import React, { Component } from 'react';
import './Providers.css';
import { generateProviders } from './providers-data';
import ProviderTable from './ProviderTable/ProviderTable';
import FeedbackWindow from "./FeedbackWindow/FeedbackWindow";


class Providers extends Component {

  constructor() {
    super();
    this.state = {
      providers: generateProviders(5),
      feedbackWindow: false
    };
  }

  openFeedbackWindow() {
    this.setState({ feedbackWindow: true });
  }

  closeFeedbackWindow() {
    this.setState({ feedbackWindow: false });
  }

  render() {


    return (
      <div className="Providers">
        <ProviderTable
          onFeedback={provider => this.openFeedbackWindow(provider)}
          providers={this.state.providers}
        />
        {this.state.feedbackWindow ? <FeedbackWindow onClose={() => this.closeFeedbackWindow()}/> : ''}
      </div>
    );
  }

}

export default Providers;
