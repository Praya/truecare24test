import React, { Component } from 'react';
import './FeedbackWindow.css';
import ModalWindow from "../../../common/ModalWindow/ModalWindow";
import Rating from "../../../common/Rating/Rating";
import ColorButton from "../../../common/ColorButton/ColorButton";

class FeedbackWindow extends Component {
  render() {
    return (
      <ModalWindow onClose={this.props.onClose} className="FeedbackWindow" title="Set the rating and provide the feedback note:">
        <Rating rating={4}/>
        <textarea placeholder="Type here"/>
        <ColorButton text="Save" color="blue"/>
      </ModalWindow>
    );
  }
}

export default FeedbackWindow;
