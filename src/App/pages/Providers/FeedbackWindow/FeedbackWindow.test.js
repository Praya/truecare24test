import React from 'react';
import ReactDOM from 'react-dom';
import FeedbackWindow from './FeedbackWindow';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<FeedbackWindow/>, div);
  ReactDOM.unmountComponentAtNode(div);
});
