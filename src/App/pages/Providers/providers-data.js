import faker from 'faker';

window['faker'] = faker;

export const statuses = [
    'contacting',
    'talked-to-client',
    'assessment-scheduled',
    'contract-signed',
    'cancel-client'
];

export function generateProviders (count) {
    return new Array(count)
        .fill(null)
        .map(() => ({
            contract: faker.random.arrayElement(['contracted', 'non-contracted']),
            type: faker.random.arrayElement(['ic', 'home-care-agency']),
            firstName: faker.name.firstName(),
            lastName: faker.name.lastName(),
            email: faker.internet.email().toLocaleLowerCase(),
            phone: faker.phone.phoneNumber('+1 (###) ###-####'),
            id: faker.random.number(1000, 9000),
            status: faker.random.arrayElement(statuses)
        }))
}
