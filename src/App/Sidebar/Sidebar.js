import React, { Component } from 'react';
import './Sidebar.css';
import SidebarIcon from './SidebarIcon/SidebarIcon';

class Sidebar extends Component {
  render() {
    return (
      <div className="Sidebar">
        <SidebarIcon icon="icons/TrueCare24_logo.svg" />
        <SidebarIcon icon="icons/dashboard.svg" title="Dashboard" />
        <SidebarIcon icon="icons/faq.svg" title="FAQ" />
        <SidebarIcon icon="icons/sign-out.svg" title="Sign out" />
      </div>
    );
  }
}

export default Sidebar;
