import React from 'react';
import ReactDOM from 'react-dom';
import SidebarIcon from './SidebarIcon';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<SidebarIcon icon="icon.png" title="title" />, div);
  ReactDOM.unmountComponentAtNode(div);
});
