import React from 'react';
import './SidebarIcon.css';

function SidebarIcon ({icon, title}) {
    return (
      <div className="SidebarIcon flex-column">
        <div className="SidebarIcon-image">
          <img src={icon} alt={title} />
        </div>
        <span className="SidebarIcon-title">{title}</span>
      </div>
    );
}

export default SidebarIcon;
