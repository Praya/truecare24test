export function makeCall(provider) {
  return fetch(process.env.REACT_APP_BACKEND_URL + '/call', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      adminPhone: process.env.REACT_APP_ADMIN_PHONE,
      provider: {
        phone: provider.phone,
        name: provider.firstName + ' ' + provider.lastName
      }
    })
  });
}
