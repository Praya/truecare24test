require('dotenv').load();
const path = require('path');
const config = require('./config');
const http = require('http');
const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const cors = require('cors');
const callRoute = require('./routes/call');
const outboundRoute = require('./routes/outbound');

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(express.static(path.join(process.cwd(), 'build')));

app.use(bodyParser.urlencoded({
    extended: true,
}));

app.use(morgan('combined'));

app.post('/call', callRoute);
app.post('/outbound/:providerPhone/:providerName', outboundRoute);

const server = http.createServer(app);

server.listen(config.port, function() {
    console.log('Express server running on port ' + config.port);
});
