const twilio = require('twilio');
const VoiceResponse = twilio.twiml.VoiceResponse;


module.exports = function(request, response) {
    const providerPhone = request.params.providerPhone;
    const providerName = request.params.providerName;
    const twimlResponse = new VoiceResponse();

    twimlResponse.say('Please wait, calling to ' + providerName, { voice: 'alice' });

    twimlResponse.dial(providerPhone);

    response.send(twimlResponse.toString());
}
