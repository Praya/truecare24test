const config = require('../config');
const twilio = require('twilio');


const client = twilio(config.accountSid, config.authToken);

module.exports = function(request, response) {
    const provider = request.body.provider;
    const adminPhone = request.body.adminPhone;

    const options = {
        to: adminPhone,
        from: config.twilioNumber,
        url: `http://${request.headers.host}/outbound/${encodeURIComponent(provider.phone)}/${encodeURIComponent(provider.name)}`,
    };

    client.calls.create(options)
      .then(() => response.status(200).send('OK'))
      .catch((error) => response.status(500).send(error));
}
