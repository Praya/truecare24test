#True Care 24 Test

![True Care 24 Test](screenshot.png "True Care 24 Test")


Task Deliverable: A demo in the form of a web page completed design + Twilio API
Task Description: based on the provided designs, make a responsive webpage + API integration (you can use a trial testing Twilio account)

1. Front end of the providers list
2. Implement the call button:
    - When call icon is clicked, first Twilio calls the phone number #1 (Admin)
    - Once admin picked up the phone, the number #2 (provider) is called. 
    - Both connected. 
3. Thing creatively of the functionality and implement 
 
Mark the task appropriately.
